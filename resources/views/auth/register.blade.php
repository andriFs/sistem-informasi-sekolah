@extends('layouts.auth')

@section('title', 'Register')


@section('content')
<div style="padding: 20%">
    <h5 class="text-danger">Registrasi akun silahkan hubungi Admin/Operator Sekolah !</h5>
    <a href="/" class="btn btn-primary">Kembali</a>
</div>
@endsection
