# Final Project

# Kelompok 25

# Anggota Kelompok

<ul>
    <li> Andri Fajar Sunandhar</li>
    <li> Farhan Nurshidik</li>
    <li> Wildan Chaerul Afif</li>
</ul>

# Tema Project

Sistem Informasi Sekolah

# ERD

<p align="center"><img src="public/assets/img/ERD2.png" width="400"></p>

# Link Video

Link Demo Aplikasi : https://drive.google.com/file/d/1Ijz0AEJy4-5cIXlV6DnuLz1Rkaeco8gq/view?usp=sharing<br>
Link Deploy : http://sistem-informasi-sekolah2.herokuapp.com/

# Akun Login Web

user : admin@admin.app <br>
password : 12345678
